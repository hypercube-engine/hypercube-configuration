module net.hypercube.configuration {
	requires java.base;

	exports net.hypercube.configuration;

	opens net.hypercube.configuration to org.mockito;
}
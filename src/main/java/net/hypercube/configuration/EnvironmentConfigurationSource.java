package net.hypercube.configuration;

import static java.util.stream.Collectors.*;

import java.util.Map;
import java.util.Optional;

/**
 * This {@link IReadOnlyConfigurationSource} is specialized on handling
 * configuration from environment variables. The names of environment variables
 * are converted {@link #EnvironmentConfigurationSource(Map)} upon
 * initialization}.
 * 
 * 
 * @author Christian Danscheid
 *
 */
public class EnvironmentConfigurationSource implements IConfigurationSource {
	protected final Map<String, String> env;

	/**
	 * Converts incoming keys from environment style (X_Y_Z) to properties style
	 * (x.y.z); Note the conversion to lowercase and the use of dots instead of
	 * underscores. Example: <code>NET_HYPERCUBE_TESTKEY</code> becomes
	 * <code>net.hypercube.testkey</code>
	 * 
	 * @param priority Priority against other configurations
	 */
	public EnvironmentConfigurationSource() {
		this.env = System.getenv().entrySet().stream()
				.collect(toMap((entry) -> entry.getKey().replace("_", ".").toLowerCase(), (entry) -> entry.getValue()));
	}

	@Override
	public <T> Optional<T> read(String key, Class<T> type) {
		return Optional.ofNullable(type.cast(env.get(key)));
	}

	@Override
	public void write(String key, String value) {
		throw new UnsupportedOperationException("Cannot write config to system environment");
	}

	@Override
	public void unset(String key) {
		throw new UnsupportedOperationException("Cannot write config to system environment");
	}
}

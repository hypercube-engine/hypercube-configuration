package net.hypercube.configuration;

import java.io.IOException;
import java.util.Optional;

public interface IConfigurationSource {
	/**
	 * Read a value from the implementation
	 * 
	 * @param key  The key
	 * @param type The return type
	 * @return value cast to type
	 */
	<T> Optional<T> read(String key, Class<T> type);

	/**
	 * Attempt to write a value to the configuration
	 * 
	 * @param key   The key
	 * @param value The value
	 * @throws IOException
	 */
	void write(String key, String value) throws IOException;

	/**
	 * Remove a value from the configuration
	 * 
	 * @param key The key
	 * @throws IOException
	 * @throws Exception
	 */
	void unset(String key) throws IOException;
}

package net.hypercube.configuration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Properties;

/**
 * Configuration read from a properties file.
 * 
 * @author Christian Danscheid
 *
 */
public class PropertiesConfigurationSource implements IConfigurationSource {
	private final Path filePath;
	private final Properties properties = new Properties();

	public PropertiesConfigurationSource(final Path path) throws IOException {
		filePath = path;
		properties.load(Files.newInputStream(path));
	}

	@Override
	public <T> Optional<T> read(String key, Class<T> type) {
		return Optional.ofNullable(type.cast(properties.get(key)));
	}

	@Override
	public void write(String key, String value) throws IOException {
		properties.setProperty(key, value);
		saveToFile();
	}

	@Override
	public void unset(String key) throws IOException {
		properties.remove(key);
		saveToFile();
	}

	private void saveToFile() throws IOException {
		properties.store(Files.newOutputStream(filePath), "");
	}
}

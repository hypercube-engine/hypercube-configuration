package net.hypercube.configuration;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class MultiSourceConfiguration {
	private final Optional<IConfigurationSource> writableConfigSource;
	private final List<IConfigurationSource> configSources;

	/**
	 * Aggregates multiple configuration sources into a single configuration.
	 * 
	 * @param configSources        The order of the sources determines their
	 *                             priority. The first entry has the highest
	 *                             priority.
	 * @param writableConfigSource The source to write changes too, optional.
	 */
	public MultiSourceConfiguration(final List<IConfigurationSource> configSources,
			final Optional<IConfigurationSource> writableConfigSource) {
		this.writableConfigSource = writableConfigSource;
		this.configSources = configSources;
	}

	public Optional<String> get(String key) {
		return get(key, String.class);
	}

	public <T> Optional<T> get(String key, Class<T> type) {
		return configSources.stream().map((src) -> src.read(key, type)).filter(Optional::isPresent).findFirst().get();
	}

	public void set(String key, String value) {
		writableConfigSource.ifPresent((source) -> {
			try {
				source.write(key, value);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}
